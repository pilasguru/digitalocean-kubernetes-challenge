output "kubeconf" {
  value     = digitalocean_kubernetes_cluster.falco.kube_config[0].raw_config
  sensitive = true
}
