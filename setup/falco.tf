provider "helm" {
  kubernetes {
    host                   = digitalocean_kubernetes_cluster.falco.kube_config[0].host
    token                  = digitalocean_kubernetes_cluster.falco.kube_config[0].token
    cluster_ca_certificate = base64decode(digitalocean_kubernetes_cluster.falco.kube_config[0].cluster_ca_certificate)
  }
}

resource "helm_release" "falco" {
  name              = "do-falco"
  repository        = "https://falcosecurity.github.io/charts"
  chart             = "falco"
  version           = "1.16.2"
  namespace         = "falco-system"
  create_namespace  = true
  dependency_update = true

  values = [
    file("values.yaml"),
  ]

  set {
    name  = "falcosidekick.enabled"
    value = "true"
  }

  set {
    name  = "falcosidekick.webui.enabled"
    value = "true"
  }

  # set {
  #   name  = "falcosidekick.config.slack.webhookurl"
  #   value = "https://hooks.slack.com/services/XXXXXX"
  # }



}
