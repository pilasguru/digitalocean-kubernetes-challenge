# DigitalOcean Kubernetes Challenge

[DigitalOcean Kubernetes Challenge](https://www.digitalocean.com/community/pages/kubernetes-challenge)

## Project Description

_Deploy a security and compliance system_
When you deploy an image you may also deploy a security threat, and images can become quickly insecure as new threats are discovered and disclosed. To continuously monitor and detect threats, you can deploy a tool like [Falco](https://falco.org/).

## Documentation

### Setup

The setup creates a Kubernetes cluster at DigitalOcean and install Falco into.

To deploy to your preferred region, add a `-var 'region=<YOUR_REGION>'` to step 3.

1. `cd setup/`
2. `terraform init`
3. `TF_VAR_do_token=<YOUR_DO_TOKEN> terraform apply`

Now obtain the kubeconfig with button `Download Config File` to access the cluster from the DigitalOcean Dashboard.

### How to use and test

#### Test A - Test initial setup

The falco-system namespace will show something like:

```
$ kubectl get all -n falco-system
NAME                                            READY   STATUS    RESTARTS   AGE
pod/do-falco-5pjzc                              1/1     Running   0          2m56s
pod/do-falco-5vkcd                              1/1     Running   0          119s
pod/do-falco-9zrmq                              1/1     Running   0          61s
pod/do-falco-falcosidekick-5d6cb855c6-86wvl     1/1     Running   0          2m59s
pod/do-falco-falcosidekick-5d6cb855c6-dqzph     1/1     Running   0          2m59s
pod/do-falco-falcosidekick-ui-76b9bc969-5pc8m   1/1     Running   0          3m

NAME                                TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/do-falco-falcosidekick      ClusterIP   10.245.37.70    <none>        2801/TCP   3m1s
service/do-falco-falcosidekick-ui   ClusterIP   10.245.91.246   <none>        2802/TCP   3m1s

NAME                      DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
daemonset.apps/do-falco   3         3         3       3            3           <none>          14d

NAME                                        READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/do-falco-falcosidekick      2/2     2            2           2m59s
deployment.apps/do-falco-falcosidekick-ui   1/1     1            1           3m

NAME                                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/do-falco-falcosidekick-5d6cb855c6     2         2         2       3m
replicaset.apps/do-falco-falcosidekick-ui-76b9bc969   1         1         1       3m1s
```

#### Test B - Test that Falco is working

Create a simple pod into any namespace:

```
$ kubectl run test-a --image ubuntu:latest sleep 5000
pod/test-a created
```

Get shell into the pod

```
$ kubectl exec -it test-a -- /bin/bash
root@nginx-test:/# id
uid=0(root) gid=0(root) groups=0(root)
root@nginx-test:/# exit
exit
```

Obtain logs from one of the pods of falco-system namespace and check 

```
$ kubectl logs do-falco-blkl9 -n falco-system | grep k8s.pod=test-a

14:24:49.846229700: Notice A shell was spawned in a container with an attached terminal (user=root user_loginuid=-1 k8s.ns=default k8s.pod=test-a container=cc32cabc1f6a shell=bash parent=runc cmdline=bash terminal=34816 container_id=cc32cabc1f6a image=docker.io/library/ubuntu) k8s.ns=default k8s.pod=test-a container=cc32cabc1f6a

14:25:16.851646498: Warning Shell history had been deleted or renamed (user=root user_loginuid=-1 type=openat command=bash fd.name=/root/.bash_history name=/root/.bash_history path=<NA> oldpath=<NA> k8s.ns=default k8s.pod=test-a container=cc32cabc1f6a) k8s.ns=default k8s.pod=test-a container=cc32cabc1f6a
```

#### Test C - Test Falco Sidekick

Open port-forward to falcosidekick-ui service

```
$ kubectl -n falco-system port-forward service/do-falco-falcosidekick-ui 2802
Forwarding from 127.0.0.1:2802 -> 2802
Forwarding from [::1]:2802 -> 2802
```

Connect to web interface [http://localhost:2802/ui](http://localhost:2802/ui)

Also check falco sidekick API

```
$ kubectl -n falco-system port-forward service/do-falco-falcosidekick 2801
Forwarding from 127.0.0.1:2801 -> 2801
Forwarding from [::1]:2801 -> 2801
```

```
$ curl -s http://localhost:2801/ping
pong
```

#### Test D - Send alerts to Slack

Go to Slack administration and configure incoming webhook to a Slack channel and obtain hook's endpoint URL.

Edit `setup/falco.tf` and uncoment `falcosidekick.config.slack.webhookurl` set with the proper URL.

Perform `terraform apply` again, as explained at [Setup](#setup) section to configure Slack integration now.

Log to any pod as root to obtain a message into your selected Slack channel like this:

```
16:04:12.830465549: Notice A shell was spawned in a container with an attached terminal
(user=root user_loginuid=-1 k8s.ns=default k8s.pod=test-a container=425c0aed9e79
shell=bash parent=runc cmdline=bash terminal=34816 container_id=425c0aed9e79
image=docker.io/library/ubuntu) k8s.ns=default k8s.pod=test-a container=425c0aed9e79
rule                                   priority
Terminal shell in container            Notice
priority                               container.id
Notice                                 425c0aed9e79
container.image.repository             k8s.ns.name
docker.io/library/ubuntu               default
k8s.pod.name                           proc.cmdline
test-a                                 bash
proc.name                              proc.pname
bash                                   runc
user.name                              time
root                                   2021-12-31 16:04:12.830465549 +0000 UTC
https://github.com/falcosecurity/falcosidekick
```

#### Test E - Test custom rules

Create a `falco_rules.local.yaml` file with your rules, like this content:

```
- rule: create_files_below_dev
  desc: creating any files below /dev other than known programs that manage devices. Some rootkits hide files in /dev.
  condition: (evt.type = creat or evt.arg.flags contains O_CREAT) and proc.name != blkid and fd.directory = /dev and fd.name != /dev/null
  output: "File created below /dev by untrusted program (user=%user.name command=%proc.cmdline file=%fd.name)"
  priority: WARNING
```

Upload the rule set to your pods with the following command (copy & paste): (see _NOTE_ at the end of this secction)

```
for i in $(kubectl get pods -n falco-system -o custom-columns=POD:metadata.name -l app=do-falco | grep -v POD); do kubectl exec -it -n falco-system $i -- mkdir -p /tmp/rules.optional.d; kubectl cp ./falco_rules.local.yaml falco-system/$i:/tmp/rules.optional.d/falco_rules.local.yaml; kubectl exec -it -n falco-system $i -- chown root: /tmp/rules.optional.d/falco_rules.local.yaml; echo $i; done
```

Get shell at any pod and create a file (i.e. `/dev/data.dat`) at the `/dev` folder.

Get falco log to check your warning:

```
$ kubectl logs -n falco-system pod/do-falco-xrwnq | grep test-a

{"output":"21:26:02.441489791: Error File created below /dev by untrusted program
(user=root user_loginuid=-1 command=bash file=/dev/data.dat container_id=46353b00c607
image=docker.io/library/ubuntu) k8s.ns=default k8s.pod=test-a container=46353b00c607 
k8s.ns=default k8s.pod=test-a container=46353b00c607","priority":"Error","rule":"Create
files below dev","source":"syscall","tags":["filesystem","mitre_persistence"],
"time":"2021-12-31T21:26:02.441489791Z", "output_fields": {"container.id":"46353b00c607",
"container.image.repository":"docker.io/library/ubuntu","evt.time":1640985962441489791,
"fd.name":"/dev/data.dat","k8s.ns.name":"default","k8s.pod.name":"test-a",
"proc.cmdline":"bash","user.loginuid":-1,"user.name":"root"}}
```

*NOTE:* Perhaps exist a better way to modify rules. The `kubectl cp` used at this example **is not for production environment use**.


## DigitalOcean Challange 

- [x] Setup a DigitalOcean Kubernetes Cluster, Falco and Falco Sidekick 
- [x] [A](#test-a-test-initial-setup) Test initial setup
- [x] [B](#test-b-test-that-falco-is-working) Test that Falco is working by execing into a pod and looking through the logs of a Falco Pod
- [x] [C](#test-c-test-falco-sidekick) Test Falco sidekick
- [x] [D](#test-d-send-alerts-to-slack) Send alerts to Slack
- [x] Configure some custom rules
- [x] [E](#test-e-test-custom-rules) Test custom rules and its alerts
- [x] Complete project and submit a PR to the kubernetes-challenge repo.
- [x] Cleanup cloud infrastructure


## Cleanup 

Cleanup all cloud infraestructure with command:

```
TF_VAR_do_token=<YOUR_DO_TOKEN> terraform destroy -auto-approve
```

## Authors and acknowledgment

The terraform code is based on [Tim Schrumpf](https://github.com/tillepille/do-k8s-challenge/)'s work for this same challange, shared with [MIT License](https://github.com/tillepille/do-k8s-challenge/blob/main/LICENSE) too.

## License

[MIT License](/LICENSE).



